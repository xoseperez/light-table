# Arduino based light table with capacitive sensors

## Hardware

### The table

* An [IKEA LACK][1] table
* A 550x550mm metracrilate board with holes in the corners
* A metalic rule and a pencil
* A tool to cut wood (a cutter might do, a Dremmel eases things a lot)
* Knots
* White glossy paint
* White adhesive paper (optional)

**Update**: You can also use an [IKEA Singlar][2] changing table, it's bigger and it's already ready to add the LED strip and plastic cover.

### The electronics

*The LED strip*

* A 5 meters 5050 led strip
* Power supply (9-12V 3A minimum, 2.1x5.5mm barrel plug, center positive)
* A remote
* It usually comes with a controller, not necessary for this project but nice to have

*For the Arduino(TM) shield and the home made controller version*

**NOTE**: This version is deprectaed, code in this repository might not work with the
schematics included in the schema folder for the shield.

* An Arduino UNO or equivalent board
* Headers to create a shield
* 2.1x5.5mm barrel
* 3 power MOSFETs
* A 1x4 low profile header to connect the LED strip
* A 32kHz IR receiver
* A 100Ohm resistor
* A 10kOhm resistor
* A 4.7uF electrolitic capacitor
* 5 5.7MOhm resistors
* 5 100pF ceramic capacitors
* Aluminium foil
* Colour stickers (black, white, red, green and blue)
* A 1x5 connection block with screws or a male header to plug the cables

*For the PCB version*

Please check the .sch and .brd files for Eagle Software in the schema folder This version supports 4 inputs, depending on which ones you want you'll have to supply the corresponding components:

* IR
* Bluetooth 2.0
* Capacitive touch
* Parallax ColorPAL

*Basics*

* A perfboard
* Solder tools
* Wires
* Something to cut the traces of the perfboard (cutter, Dremmel)
* ...

## The code

The project is ready to be build using [PlatformIO][3], please refer to their site on instructions to install it. Once installed you just have to:

```bash
> cd code
> platformio run
> platformio run --target upload
```
Library dependencies are automatically managed via PlatformIO Library Manager.

## Supported BT apps

* [Color LED Controller][4] by Ryan Chen
* [Arduino Bluetooth RGB][5] LEDs by Amphan



[1]: http://www.ikea.com/es/es/catalog/products/80193735/
[2]: http://www.ikea.com/es/es/catalog/products/20045205/
[3]: http://www.platformio.org
[4]: https://play.google.com/store/apps/details?id=appinventor.ai_yuanryan_chen.BT_LED
[5]: https://play.google.com/store/apps/details?id=arduino.bluetooth.rgbleds
