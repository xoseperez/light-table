/*

  Light Table
  Copyright (C) 2013-2015 by Xose Pérez <xose dot perez at gmail dot com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include <SoftwareSerial.h>
#include <CapacitiveSensor.h>
#include <IRremote.h>

// ===========================================
// Configuration
// ===========================================

#define SERIAL_BAUDRATE     9600
#define BLUETOOTH_BAUDRATE  9600
#define COLORPAL_BAUDRATE   4800

#define SENSOR_SEND_PIN     14 // A0
#define SENSOR_OFF_PIN      15 // A1
#define SENSOR_WHITE_PIN    16 // A2
#define SENSOR_RED_PIN      17 // A3
#define SENSOR_GREEN_PIN    18 // A4
#define SENSOR_BLUE_PIN     19 // A5
#define LEDSTRIP_RED_PIN    9
#define LEDSTRIP_GREEN_PIN  6
#define LEDSTRIP_BLUE_PIN   10
#define IR_PIN              8
#define SWITCH_PIN          11
#define BLUETOOTH_TX        13
#define BLUETOOTH_RX        12
#define COLORPAL_PIN        4

#define SENSOR_UPPER_THRESHOLD 400
#define SENSOR_LOWER_THRESHOLD 50

#define BUTTON_COUNT        5
#define BUTTON_OFF          0
#define BUTTON_WHITE        1
#define BUTTON_RED          2
#define BUTTON_GREEN        3
#define BUTTON_BLUE         4

#define DEFAULT_BRIGHTNESS  0xC0 // 192
#define COLOR_STEP          8
#define TRANSITION_NONE     0
#define TRANSITION_STEP     1

#define CAPSENSE_READ_TIME      10
#define CAPSENSE_CHECK_PERIOD   500
#define CAPSENSE_TIMEOUT        50

#define SERIAL_BUFFER_SIZE  20

#define STATE_UNPRESSED     0
#define STATE_PRESSED       1
#define EVENT_UNCHANGED     0
#define EVENT_RISING        1
#define EVENT_FALLING       2

#define CODE_OFF            0x40BF
#define CODE_ON             0xC03F
#define CODE_BRIGHT_MINUS   0x807F
#define CODE_BRIGHT_PLUS    0x00FF
#define CODE_FLASH          0xD02F
#define CODE_STROBE         0xF00F
#define CODE_FADE           0xC837
#define CODE_SMOOTH         0xE817
#define CODE_RED            0x20DF
#define CODE_GREEN          0xA05F
#define CODE_BLUE           0x609F
#define CODE_WHITE          0xE01F
#define CODE_ORANGE         0x10EF
#define CODE_ORANGE_LIGHT   0x30CF
#define CODE_BROWN          0x08F7
#define CODE_YELLOW         0x28D7
#define CODE_GREEN_LIGHT    0x906F
#define CODE_GREEN_BLUE1    0xB04F
#define CODE_GREEN_BLUE2    0x8877
#define CODE_GREEN_BLUE3    0xA857
#define CODE_BLUE_LIGHT     0x50AF
#define CODE_PURPLE_DARK    0x708F
#define CODE_PURPLE_LIGHT   0x48B7
#define CODE_PINK           0x6897
#define CODE_NOISE          0xFFFF

#define COLOR_RED           (RGB) {0xFF, 0x00, 0x00}
#define COLOR_GREEN         (RGB) {0x00, 0xFF, 0x00}
#define COLOR_BLUE          (RGB) {0x00, 0x00, 0xFF}
#define COLOR_WHITE         (RGB) {0xFF, 0xFF, 0xFF}
#define COLOR_BLACK         (RGB) {0x00, 0x00, 0x00}
#define COLOR_ORANGE        (RGB) {0xFF, 0x7F, 0x00}
#define COLOR_ORANGE_LIGHT  (RGB) {0xFF, 0xAA, 0x00}
#define COLOR_BROWN         (RGB) {0xFF, 0xD4, 0x00}
#define COLOR_YELLOW        (RGB) {0xFF, 0xFF, 0x00}
#define COLOR_GREEN_LIGHT   (RGB) {0x00, 0xFF, 0xAA}
#define COLOR_GREEN_BLUE1   (RGB) {0x00, 0xFF, 0xFF}
#define COLOR_GREEN_BLUE2   (RGB) {0x00, 0xAA, 0xFF}
#define COLOR_GREEN_BLUE3   (RGB) {0x00, 0x55, 0xFF}
#define COLOR_BLUE_LIGHT    (RGB) {0x00, 0x00, 0x80}
#define COLOR_PURPLE_DARK   (RGB) {0x3F, 0x00, 0x80}
#define COLOR_PURPLE_LIGHT  (RGB) {0x7A, 0x00, 0xBF}
#define COLOR_PINK          (RGB) {0xFF, 0x00, 0xFF}

// Note:
// Not all methods can work at the same time
// BLUETOOTH and COLORPAL both user SoftwareSerial
// thus only one can be listening at a given time
// When both are enabled the hardware switch
// controls who is listening (closed being bluetooth)
// On the other hand CAPSENSE takes a lot of time
// so it might be a good idea to enabled it via
// the hardware switch too.

#define DEBUG
#define ENABLE_IR
//#define ENABLE_CAPSENSE
#define ENABLE_BLUETOOTH
#define ENABLE_COLORPAL

// ===========================================
// Globals
// ===========================================

#ifdef ENABLE_CAPSENSE
    CapacitiveSensor csOff = CapacitiveSensor(SENSOR_SEND_PIN, SENSOR_OFF_PIN);
    CapacitiveSensor csWhite =  CapacitiveSensor(SENSOR_SEND_PIN, SENSOR_WHITE_PIN);
    CapacitiveSensor csRed =  CapacitiveSensor(SENSOR_SEND_PIN, SENSOR_RED_PIN);
    CapacitiveSensor csGreen =  CapacitiveSensor(SENSOR_SEND_PIN, SENSOR_GREEN_PIN);
    CapacitiveSensor csBlue =  CapacitiveSensor(SENSOR_SEND_PIN, SENSOR_BLUE_PIN);
    CapacitiveSensor button[BUTTON_COUNT] = {csOff, csWhite, csRed, csGreen, csBlue};
    unsigned long capsense_timer = millis();
    boolean capsense_enabled = true;
#endif

#ifdef ENABLE_IR
    IRrecv irrecv(IR_PIN);
#endif

#ifdef ENABLE_BLUETOOTH
    SoftwareSerial bt(BLUETOOTH_RX, BLUETOOTH_TX);
    char buffer[SERIAL_BUFFER_SIZE];
    byte pointer = 0;
#endif

#ifdef ENABLE_COLORPAL
    SoftwareSerial cpIN(COLORPAL_PIN, 255);
    SoftwareSerial cpOUT(255, COLORPAL_PIN);
#endif

struct RGB {
    uint8_t red;
    uint8_t green;
    uint8_t blue;
};

struct Effect {
    RGB * colors;
    uint8_t size;
    uint8_t type;
    unsigned long transition_delay;
    unsigned long transition_pause;
};

RGB color = COLOR_BLACK;
uint8_t brightness = DEFAULT_BRIGHTNESS;
boolean switch_on = false;

// Effects
boolean effect_enabled = false;
unsigned long effect_timer = 0;
Effect effect;
RGB colors_flash[2] = { COLOR_WHITE, COLOR_BLACK };
RGB colors_strobe[3] = { COLOR_RED, COLOR_GREEN, COLOR_BLUE };
RGB colors_smooth[7] = { COLOR_RED, COLOR_ORANGE, COLOR_YELLOW, COLOR_GREEN, COLOR_BLUE, COLOR_PURPLE_LIGHT, COLOR_PINK  };

// ===========================================
// Methods
// ===========================================

/*
 * updateLedStrip
 * Updates all LEDs to te current color
 *
 * @return void
 */
void updateLedStrip() {

    analogWrite(LEDSTRIP_RED_PIN, map(color.red, 0, 0xFF, 0, brightness));
    analogWrite(LEDSTRIP_GREEN_PIN, map(color.green, 0, 0xFF, 0, brightness));
    analogWrite(LEDSTRIP_BLUE_PIN, map(color.blue, 0, 0xFF, 0, brightness));

    #ifdef DEBUG
        Serial.print("RED: ");
        Serial.print(color.red);
        Serial.print("\tGREEN: ");
        Serial.print(color.green);
        Serial.print("\tBLUE: ");
        Serial.print(color.blue);
        Serial.print("\tBRIGHTNESS: ");
        Serial.println(brightness);
    #endif

}

/*
 * effectProcess
 * Light effects management method
 *
 * @return void
 */
void effectProcess(boolean reset = false) {

    static uint8_t count = 0;

    if (reset) count = 0;

    RGB destination = effect.colors[count];

    switch(effect.type) {

        case TRANSITION_STEP: {
            if (color.red == destination.red && color.green == destination.green && color.blue == destination.blue) {
                effect_timer = millis() + effect.transition_pause;
                count = (count + 1) % effect.size;
            } else {
                uint8_t delta;
                delta = min(abs(destination.red - color.red), COLOR_STEP);
                color.red = color.red > destination.red ? color.red - delta : color.red + delta;
                delta = min(abs(destination.green - color.green), COLOR_STEP);
                color.green = color.green > destination.green ? color.green - delta : color.green + delta;
                delta = min(abs(destination.blue - color.blue), COLOR_STEP);
                color.blue = color.blue > destination.blue ? color.blue - delta : color.blue + delta;
                effect_timer = millis() + effect.transition_delay;
            }
            break;
        }

        case TRANSITION_NONE:
        default:
            color = destination;
            effect_timer = millis() + effect.transition_pause;
            count = (count + 1) % effect.size;
            break;
    }

    updateLedStrip();

}

#ifdef ENABLE_CAPSENSE

    /*
     * getButtonState
     * Returns the button state
     * Uses a schmitt trigger-like approach to avoid bouncing
     * Fine tune SENSOR_UPPER_THRESHOLD and SENSOR_LOWER_THRESHOLD
     * to match your case
     *
     * @param uint8_t button_id in the CapacitiveSensor button array
     * @return unit8_t STATE_PRESSED || STATE_UNPRESSED
     */
    uint8_t getButtonState(uint8_t button_id) {

        static uint8_t state[BUTTON_COUNT] = { STATE_UNPRESSED };
        long value = button[button_id].capacitiveSensor(CAPSENSE_READ_TIME);
        if (value > SENSOR_UPPER_THRESHOLD) {
            state[button_id] = STATE_PRESSED;
        } else if (value < SENSOR_LOWER_THRESHOLD) {
            state[button_id] = STATE_UNPRESSED;
        }
        return state[button_id];

    }

    /*
     * getButtonEvent
     * Returns the button event
     * Reports any transition from the previous state check
     *
     * @param uint8_t button_id in the CapacitiveSensor button array
     * @return unit8_t EVENT_NONE || EVENT_RISING || EVENT_FALLING
     */
    uint8_t getButtonEvent(uint8_t button_id) {

        static uint8_t previousState[BUTTON_COUNT] = { STATE_UNPRESSED };
        uint8_t newState = getButtonState(button_id);
        if (newState != previousState[button_id]) {
            previousState[button_id] = newState;
            if (newState == STATE_PRESSED) {
                return EVENT_RISING;
            } else {
                return EVENT_FALLING;
            }
        }
        return EVENT_UNCHANGED;

    }

    /*
     * checkCapacitiveSensors
     * In order to update table colors one must press and hold the
     * check button and then press only one of the color buttons.
     * Color button WHITE toggles on and off all colors, the other
     * three buttons toggles on and off each color.
     */
    void checkCapacitiveSensors() {

        bool update = false;

        // Only one color button pressed at any given time
        bool off = getButtonEvent(BUTTON_OFF) == EVENT_RISING;
        bool white = getButtonEvent(BUTTON_WHITE) == EVENT_RISING;
        bool red = getButtonEvent(BUTTON_RED) == EVENT_RISING;
        bool green = getButtonEvent(BUTTON_GREEN) == EVENT_RISING;
        bool blue = getButtonEvent(BUTTON_BLUE) == EVENT_RISING;

        if (off & !white && !red && !green && !blue) {
            color = COLOR_BLACK;
            update = true;
        }

        if (!off & white && !red && !green && !blue) {
            color = COLOR_WHITE;
            update = true;
        }

        if (!off & !white && red && !green && !blue) {
            color = COLOR_RED;
            update = true;
        }

        if (!off & !white && !red && green && !blue) {
            color = COLOR_GREEN;
            update = true;
        }

        if (!off & !white && !red && !green && blue) {
            color = COLOR_BLUE;
            update = true;
        }

        if (update) updateLedStrip();

        #ifdef DEBUG
            Serial.print(button[BUTTON_OFF].capacitiveSensor(CAPSENSE_READ_TIME));
            Serial.print("\t");
            Serial.print(button[BUTTON_WHITE].capacitiveSensor(CAPSENSE_READ_TIME));
            Serial.print("\t");
            Serial.print(button[BUTTON_RED].capacitiveSensor(CAPSENSE_READ_TIME));
            Serial.print("\t");
            Serial.print(button[BUTTON_GREEN].capacitiveSensor(CAPSENSE_READ_TIME));
            Serial.print("\t");
            Serial.print(button[BUTTON_BLUE].capacitiveSensor(CAPSENSE_READ_TIME));
            Serial.println("");
        #endif

    }

#endif

#ifdef ENABLE_IR

    /*
     * checkIRSensor
     */
    void checkIRSensor() {

        decode_results results;
        if (irrecv.decode(&results)) {

            boolean was_effect_enabled = effect_enabled;
            boolean update = true;
            effect_enabled = false;

            #ifdef DEBUG
                //Serial.print("IR Code: ");
                //Serial.println(results.value, HEX);
            #endif

            switch(results.value & 0xFFFF) {

                case CODE_ON:           color = COLOR_WHITE; break;
                case CODE_OFF:          color = COLOR_BLACK; break;

                case CODE_BRIGHT_MINUS:
                    brightness = constrain(brightness - COLOR_STEP, 0, 0xFF);
                    effect_enabled = was_effect_enabled;
                    break;

                case CODE_BRIGHT_PLUS:
                    brightness = constrain(brightness + COLOR_STEP, 0, 0xFF);
                    effect_enabled = was_effect_enabled;
                    break;

                case CODE_RED:          color = COLOR_RED; break;
                case CODE_GREEN:        color = COLOR_GREEN; break;
                case CODE_BLUE:         color = COLOR_BLUE; break;
                case CODE_WHITE:        color = COLOR_WHITE; break;

                case CODE_ORANGE:       color = COLOR_ORANGE; break;
                case CODE_ORANGE_LIGHT: color = COLOR_ORANGE_LIGHT; break;
                case CODE_BROWN:        color = COLOR_BROWN; break;
                case CODE_YELLOW:       color = COLOR_YELLOW; break;

                case CODE_GREEN_LIGHT:  color = COLOR_GREEN_LIGHT; break;
                case CODE_GREEN_BLUE1:  color = COLOR_GREEN_BLUE1; break;
                case CODE_GREEN_BLUE2:  color = COLOR_GREEN_BLUE2; break;
                case CODE_GREEN_BLUE3:  color = COLOR_GREEN_BLUE3; break;

                case CODE_BLUE_LIGHT:   color = COLOR_BLUE_LIGHT; break;
                case CODE_PURPLE_DARK:  color = COLOR_PURPLE_DARK; break;
                case CODE_PURPLE_LIGHT: color = COLOR_PURPLE_LIGHT; break;
                case CODE_PINK:         color = COLOR_PINK; break;

                case CODE_FLASH: {
                    colors_flash[0] = color;
                    effect = (Effect) { colors_flash, 2, TRANSITION_NONE, 0, 1000 };
                    effect_enabled = true;
                    effectProcess(true);
                    break;
                }

                case CODE_STROBE: {
                    effect = (Effect) { colors_strobe, 3, TRANSITION_NONE, 0, 1000 };
                    effect_enabled = true;
                    effectProcess(true);
                    break;
                }

                case CODE_FADE: {
                    colors_flash[0] = color;
                    effect = (Effect) { colors_flash, 2, TRANSITION_STEP, 200, 0 };
                    effect_enabled = true;
                    effectProcess(true);
                    break;
                }

                case CODE_SMOOTH: {
                    effect = (Effect) { colors_smooth, 7, TRANSITION_STEP, 200, 0 };
                    effect_enabled = true;
                    effectProcess(true);
                    break;
                }

                default:
                    effect_enabled = was_effect_enabled;
                    update = false;
                    break;

            }

            if (update) updateLedStrip();
            irrecv.resume(); // Receive the next value

        }

    }

#endif

#ifdef ENABLE_BLUETOOTH

    /**
     * checkSerial
     * This method parses messages from Arduino Bluetooth RGB LEDs app from Android
     * https://play.google.com/store/apps/details?id=arduino.bluetooth.rgbleds
     */
    void checkSerial()  {

        if (bt.available()>0) {

            char c = bt.read();
            Serial.print(c);

            if (c == ')') {

                // close the buffer
                buffer[pointer++] = 0;

                if (strcmp(buffer, "ON") == 0) {
                    color.red = color.green = color.blue = 255;
                } else if (strcmp(buffer, "OFF") == 0) {
                    color.red = color.green = color.blue = 0;
                } else {

                    char * str;
                    int red = 0, green = 0, blue = 0;

                    if (str = strtok(buffer, ".")) red = atoi(str);
                    if (str = strtok(NULL, ".")) green = atoi(str);
                    if (str = strtok(NULL, ".")) blue = atoi(str);

                    color.red = constrain(red, 0, 255);
                    color.green = constrain(green, 0, 255);
                    color.blue = constrain(blue, 0, 255);

                }

                updateLedStrip();
                pointer = 0;

            } else if (c == 10) {

               // close the buffer
               buffer[pointer++] = 0;

               char * str;
               int red = 0, green = 0, blue = 0;

               if (str = strtok(buffer, ",")) red = atoi(str);
               if (str = strtok(NULL, ",")) green = atoi(str);
               if (str = strtok(NULL, ",")) blue = atoi(str);

               color.red = map(red, 0, 100, 0, 255);
               color.green = map(green, 0, 100, 0, 255);
               color.blue = map(blue, 0, 100, 0, 255);

               updateLedStrip();
               pointer = 0;

            } else {
                buffer[pointer++] = c;
                if (pointer == SERIAL_BUFFER_SIZE) pointer=0;
            }

        }
    }

#endif

#ifdef ENABLE_COLORPAL

    void setupColorPAL() {

        // Reset
        pinMode(COLORPAL_PIN, OUTPUT);
        digitalWrite(COLORPAL_PIN, LOW);
        pinMode(COLORPAL_PIN, INPUT);
        while (digitalRead(COLORPAL_PIN) != HIGH);
        pinMode(COLORPAL_PIN, OUTPUT);
        digitalWrite(COLORPAL_PIN, LOW);
        delay(80);
        pinMode(COLORPAL_PIN, INPUT);
        delay(200);

        // Configure softserial comm to ColorPAL
        cpOUT.begin(COLORPAL_BAUDRATE);
        pinMode(COLORPAL_PIN, OUTPUT);
        cpOUT.print("= (00 $ m p64) !");    // Infinite loop print values with a ~500ms pause between them
        cpOUT.end();                        // Discontinue serial port for transmitting

    }

    void checkColorPAL() {

        char buffer[32];
        int red;
        int green;
        int blue;

        if (cpIN.available() > 0) {

            // Wait for a $ character, then read three 3 digit hex numbers
            buffer[0] = cpIN.read();
            if (buffer[0] == '$') {

                for(int i = 0; i < 9; i++) {
                    while (cpIN.available() == 0);     // Wait for next input character
                    buffer[i] = cpIN.read();
                    if (buffer[i] == '$')               // Return early if $ character encountered
                        return;
                }

                sscanf (buffer, "%3x%3x%3x", &red, &green, &blue);
                if (red>0 and green>0 and blue>0) {
                    #ifdef DEBUG
                        Serial.print("ColorPAL output: ");
                        Serial.print(red);
                        Serial.print(" ");
                        Serial.print(green);
                        Serial.print(" ");
                        Serial.println(blue);
                    #endif
                    color.red = constrain(map(red, 40, 180, 0, 255), 0, 255);
                    color.green = constrain(map(green, 40, 200, 0, 255), 0, 255);
                    color.blue = constrain(map(blue, 50, 266, 0, 255), 0, 255);
                    updateLedStrip();
                }

                delay(10);

            }
        }


    }


#endif
/*
 * setup
 *
 * @return void
 */
void setup() {

    // Initialize UART
    #ifdef DEBUG
        Serial.begin(SERIAL_BAUDRATE);
    #endif

    #ifdef ENABLE_BLUETOOTH
        // This takes place in the debounce method in the loop
        // bt.begin(BLUETOOTH_BAUDRATE);
    #endif

    #ifdef ENABLE_COLORPAL
        setupColorPAL();
    #endif

    // Initialize ledstrip outputs
    pinMode(LEDSTRIP_RED_PIN, OUTPUT);
    pinMode(LEDSTRIP_GREEN_PIN, OUTPUT);
    pinMode(LEDSTRIP_BLUE_PIN, OUTPUT);

    // Initialize hardware switch
    pinMode(SWITCH_PIN, INPUT_PULLUP);

    #ifdef ENABLE_CAPSENSE
        for (uint8_t i=0; i<BUTTON_COUNT; i++) {
            button[i].set_CS_Timeout_Millis(CAPSENSE_TIMEOUT);
        }
    #endif

    // Start the IR receiver
    #ifdef ENABLE_IR
        irrecv.enableIRIn();
    #endif

    // Initialize LED Strip
    updateLedStrip();

}

/*
 * loop
 *
 * @return void
 */
void loop() {

    unsigned long time = millis();

    // Debounce switch
    if (switch_on != (digitalRead(SWITCH_PIN) == LOW)) {
        delay(50);
        if (switch_on != (digitalRead(SWITCH_PIN) == LOW)) {

            switch_on = ! switch_on;

            #ifdef ENABLE_CAPSENSE
                capsense_timer = time + CAPSENSE_CHECK_PERIOD;
            #endif

            #ifdef ENABLE_BLUETOOTH
                if (!switch_on) {
                    bt.begin(BLUETOOTH_BAUDRATE);
                } else {
                    bt.end();
                }
            #endif

            #ifdef ENABLE_COLORPAL
                if (!switch_on) {
                    cpIN.end();
                } else {
                    cpIN.begin(COLORPAL_BAUDRATE);      // Set up serial port for receiving
                    pinMode(COLORPAL_PIN, INPUT);
                }
            #endif


        }
    }

    #ifdef ENABLE_IR
        checkIRSensor();
    #endif

    #ifdef ENABLE_CAPSENSE
        if (switch_on) {
            if (capsense_timer < time) {
                checkCapacitiveSensors();
                capsense_timer = time + CAPSENSE_CHECK_PERIOD;
            }
        }
    #endif

    #ifdef ENABLE_BLUETOOTH
        if (!switch_on) {
            checkSerial();
        }
    #endif

    #ifdef ENABLE_COLORPAL
        if (switch_on) {
            checkColorPAL();
        }
    #endif

    if (effect_enabled && (effect_timer < time)) {
        effectProcess();
    }


}
